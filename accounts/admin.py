from django.contrib import admin
from .models import User,Departments

class UserAdmin(admin.ModelAdmin):
    search_fields = ['email']
    list_display = ('id', 'username', 'email', 'is_superuser', 'is_staff', 'department_id')


admin.site.register(User, UserAdmin)
admin.site.register(Departments)


# Register your models here.
