from django.urls import path,include
#from .views import views_simizu,views_saito,views_nihei,views_ise,views_araya
from .views import views_nihei

app_name = 'report'

urlpatterns = [
    #path('',views.IndexView.as_view(),name='index'),
    path('company_list/',views_nihei.CompanyListView.as_view(),name="company_list"),
    path('company_list2/',views_nihei.CompanyListTwoView.as_view(),name="company_list2"),
    path('company_register/',views_nihei.CompanyRegisterView.as_view(),name="company_register"),
    path('tag_register/',views_nihei.TagRegisterView.as_view(),name="tag_register"),
    path('company_detail/<int:pk>',views_nihei.CompanyDetailView.as_view(),name="company_detail"),
    path('offer_register/<int:pk>',views_nihei.OfferRegisterView.as_view(),name="offer_register"),
    path('tag_delete/', views_nihei.TagDeleteView.as_view(),name="tag_delete"),
    path('offer_delete/<int:pk>',views_nihei.OfferDeleteView.as_view(),name="offer_delete"),
    path('company_add_tag/<int:pk>',views_nihei.CompanyAddTagView.as_view(),name="company_add_tag"),
    path('company_register_tag/<int:pk>',views_nihei.CompanyRegisterTagView.as_view(),name="company_register_tag"),
    path('company_register_occupation/<int:pk>',views_nihei.CompanyRegisterOccupationView.as_view(),name="company_register_occupation"),
    path('letter_template/<int:pk>',views_nihei.LetterTemplateView.as_view(),name="letter_template"),
    path('create_exam_report/<int:pk>',views_nihei.CreateExamReportView.as_view(),name="create_exam_report"),
]