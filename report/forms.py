from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
User = get_user_model()
import datetime
from accounts.models import User,Departments
from django.core.exceptions import ValidationError
import re

def check_email(value):
    # pattern = re.compile(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
    pattern = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
    if 'admin' in value:
        raise ValidationError('メールアドレスにadminは使わないでください')
    elif not re.match(pattern, value):
        raise ValidationError('不適切なメールアドレスです')

def check_password(value):
    if len(value) <= 7:
        raise ValidationError('パスワードは８文字以上にしてください')


class SignUpForm(UserCreationForm):
    # password = forms.CharField()
    username = forms.CharField(label='名前')
    email = forms.EmailField(label='メールアドレス', validators=[check_email] )
    password = forms.CharField(label='パスワード', validators=[check_password],widget=forms.PasswordInput)
    password1 = forms.CharField(required=False, widget=forms.HiddenInput)
    password2 = password1
    class Meta:
        model = User
        fields = ('department',)

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['department'].queryset = Departments.objects.filter(department_date = None)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'

class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            self.fields['username'].widget.attrs['class'] = 'form-control'
            self.fields['password'].widget.attrs['class'] = 'form-control'




