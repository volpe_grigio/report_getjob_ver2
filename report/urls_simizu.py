from django.urls import path,include
from .views import views_simizu
from .views import views_araya,views_nihei,views_ise


app_name = 'report'
urlpatterns = [
    path('', views_simizu.MenuDisplayView.as_view(), name="menu"),
]
""" 
    path('details_report/<int:pk>', views_araya.ReportDetailView.as_view(), name="details_report"),
    path('edit_report/', views_araya.ReportEditView.as_view(), name="edit_report"),
    path('my_report/<int:pk>', views_araya.ReportListView.as_view(), name="my_report"),
    path('register_report/', views_araya.ReportRegisterView.as_view(), name="register_report"),
"""