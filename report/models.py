from django.core.validators import FileExtensionValidator
from django.db import models
from accounts.models import Departments,User
from django.contrib.postgres.fields import ArrayField

# Create your models here.

#--------------------------------------------------------------1
#タグ
class Tag(models.Model):
    tag_name = models.CharField(verbose_name="タグ名",max_length=50,unique=True)

    class Meta:
        verbose_name_plural = "Tag"

    def __str__(self):
        return self.tag_name
#-------------------------------------------------------------2
#業種（大分類）
class Big_Industry(models.Model):
    big_id = models.CharField(verbose_name="大分類ID",primary_key=True,unique=True,max_length=2)
    big_name = models.CharField(verbose_name="大分類名称", max_length=30, unique=True)

    class Meta:
        verbose_name_plural = "Big_Industry"

    def __str__(self):
        return self.big_name

#------------------------------------------------------------3
#業種（中分類）
class Middle_Industry(models.Model):
    middle_name = models.CharField(verbose_name="中分類名称",unique=True,max_length=30)
    big = models.ForeignKey(Big_Industry,verbose_name="大分類名称",on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Middle_Industry"

    def __str__(self):
        return self.middle_name
# ------------------------------------------------------------4
#職種
class Occupation(models.Model):
    occupation_name = models.CharField(verbose_name="職種名",max_length=40,unique=True)
    middle = models.ForeignKey(Middle_Industry,verbose_name="職業ID", on_delete=models.PROTECT,null=True)

    class Meta:
        verbose_name_plural = "Occupation"

    def __str__(self):
        return self.occupation_name
#----------------------------------------------------------5
#資格
class Qualification(models.Model):
    qualif_name = models.CharField(verbose_name="資格名",max_length=60,unique=True)

    class Meta:
        verbose_name_plural = 'Qualification'

    def __str__(self):
        return self.qualif_name

#-------------------------------------------------------------5
#資格と学科
#class Qualif_Depart(models.Model):
    #qualif = models.ForeignKey(Qualification,verbose_name="資格ID",on_delete=models.PROTECT)
    #depart = models.ForeignKey(Departments,verbose_name="関連学科ID",on_delete=models.PROTECT)
    #class Meta:
     #   verbose_name_plural = 'Qualif_Depart'
    #def __str__(self):
     #   return self.qualif_id,self.depart_id
# --------------------------------------------------------------6
# 企業
class Company(models.Model):
    company_name = models.CharField(verbose_name="企業名", max_length=300)
    kana_name = models.CharField(verbose_name="カナ企業名", max_length=1000)
    kana_name_normalized = models.CharField(verbose_name="カナ企業名＿正規化", max_length=1000)
    address_number = models.CharField(verbose_name="郵便番号", max_length=10)
    locate = models.CharField(verbose_name="会社所在地", max_length=200)

    class Meta:
        verbose_name_plural = "Company"

    def __str__(self):
        return self.company_name
#-----------------------------------------------------------7
##資格と企業
class Qualif_Compa(models.Model):
    qualif = models.ForeignKey(Qualification,verbose_name="資格ID",on_delete=models.PROTECT)
    company = models.ForeignKey(Company,verbose_name="企業ID", on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = 'Qualif_Compa'
#---------------------------------------------------------------8
#求人
class Offers(models.Model):
    company = models.ForeignKey(Company,verbose_name="企業ID",on_delete=models.PROTECT)
    occupation = models.ForeignKey(Occupation,verbose_name="募集職種ID",on_delete=models.PROTECT)
    offer_pdf = models.FileField(upload_to='uploads',verbose_name="求人票",validators=[FileExtensionValidator(['pdf',])],blank=True,null=True)
    year = models.DateField(verbose_name="西暦年度",auto_now_add=True,null=True)

    class Meta:
        verbose_name_plural = "Offers"
        #unique_together = ("company_id","occupation_id")
#---------------------------------------------------------------9
#試験報告
class Exam_Report(models.Model):
    PASSED_CHOICES = (
        (True,"合格"),
        (False,"不合格"),
    )
    TEST_CHOICES = (
        ("1","１次試験"),
        ("2","２次試験"),
        ("3","３次試験"),
        ("4","４次試験"),
        ("5","５次試験"),
        ("last","最終試験"),
    )
    report_passed = models.BooleanField(choices=PASSED_CHOICES,verbose_name="合否",null=True,blank=True)
    report_company = models.ForeignKey(Company,verbose_name="受験企業",on_delete=models.PROTECT)
    report_user = models.ForeignKey(User,verbose_name="受験者",on_delete=models.PROTECT)
    report_date = models.DateField(verbose_name="登録日",auto_now_add=True)
    report_pdf = models.FileField(upload_to='report_uploads/%Y/%m/',verbose_name="受験報告書", validators=[FileExtensionValidator(['pdf',])], blank=True,null=True)
    report_tests = models.CharField(verbose_name="試験回数",choices=TEST_CHOICES,max_length=4,null=True,blank=True)

    class Meta:
        verbose_name_plural = "Exam_Report"
#-------------------------------------------------10
#タグと企業
class Tag_Company(models.Model):
    company = models.ForeignKey(Company,verbose_name="企業名",on_delete=models.PROTECT)
    tag = models.ForeignKey(Tag,verbose_name="タグ名", on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name_plural = "Company_Tag"
        constraints = [models.UniqueConstraint(
                    fields=["company_id","tag_id"],
                    name="company_tag_unique"),
        ]
#---------------------------------------------------11
#業種（大分類）と企業
class Company_Industry(models.Model):
    company = models.ForeignKey(Company,verbose_name="企業ID",on_delete=models.PROTECT)
    industry = models.ForeignKey(Big_Industry,verbose_name="業種ID",on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Company_Industry"
        constraints = [models.UniqueConstraint(
            fields=["company_id","industry_id"],
            name="company_industry_unique"),
        ]
#-----------------------------------------------------12
#資格と学科
class Qualif_Departments(models.Model):
    qualification = models.ForeignKey(Qualification,verbose_name="資格ID",on_delete=models.PROTECT)
    departments = models.ForeignKey(Departments,verbose_name="学科ID",on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Qualification_Departments"
        constraints = [models.UniqueConstraint(
            fields=["qualification_id", "departments_id"],
            name="qualification_departments_unique"),
        ]
#----------------------------------------------------------