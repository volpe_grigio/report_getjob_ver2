from django.urls import path,include
#from report.views import views_simizu,views_saito,views_nihei,views_ise,views_araya
from .views import views_araya

app_name = 'report'

urlpatterns = [
    #path('',views.IndexView.as_view(),name='index'),
    path('details_report/<int:pk>', views_araya.ReportDetailView.as_view(), name="details_report"),
    path('edit_report/<int:pk>', views_araya.ReportEditView.as_view(), name="edit_report"),
    #path('my_report/', views_araya.ReportListView.as_view(), name="my_report"),
    path('register_report/', views_araya.ReportRegisterView.as_view(), name="register_report"),
    path('my_report/<int:pk>', views_araya.ReportListView.as_view(), name="my_report"),
]