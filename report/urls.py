from django.urls import path
from report.views import views_simizu,views_saito,views_nihei,views_ise,views_araya

from .urls_araya import urlpatterns as urls_araya
from .urls_ise import urlpatterns as urls_ise
from .urls_nihei import urlpatterns as urls_nihei

from .urls_simizu import urlpatterns as urls_simizu
from .urls_saito import urlpatterns as urls_saito

app_name = 'report'

urlpatterns = [
    path('index/',views_saito.IndexView.as_view(),name='index'),
]

urlpatterns.extend(urls_araya)
urlpatterns.extend(urls_ise)
urlpatterns.extend(urls_nihei)
urlpatterns.extend(urls_simizu)
urlpatterns.extend(urls_saito)
