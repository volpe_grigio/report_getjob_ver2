from django.shortcuts import render, resolve_url
from django.views import generic
from django.urls import reverse_lazy
from ..models import *
from django.urls import reverse
from ..forms_araya import *
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse,HttpResponseRedirect
from .Mixin import *

# Create your views here.

class ReportListView(onlyyouMixin,LoginRequiredMixin, generic.ListView):
    model = Exam_Report
    template_name = "templates_araya/my_report.html"
    paginate_by = 5
    login_url = 'report:login'

    def get_context_data(self, **kwargs):
        exam_report_context = super().get_context_data(**kwargs)
        client = self.request.user
        print(client.id)
        if client.department_id != 6:report_list = Exam_Report.objects.filter(report_user_id=client.id)
        else:report_list = Exam_Report.objects.all().order_by('report_company_id','report_tests','report_passed','report_date',)
        exam_report_context.update({
            "report_list":report_list,
            "user":client,
        })

        return exam_report_context

class ReportRegisterView(generic.CreateView):
    model = Exam_Report,Company
    form_class = CreateReportForm
    template_name = "templates_araya/register_report.html"
    success_url = reverse_lazy('report:my_report')

    def get_success_url(self):
        return reverse('report:my_report',kwargs={'pk':self.request.user.id})

    def form_valid(self, form):
        report = form.save(commit=False)
        report.report_user = self.request.user
        report.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "失敗しました。")
        return super().form_invalid(form)

class ReportDetailView(generic.TemplateView):
    model = Company, Exam_Report
    template_name = "templates_araya/details_report.html"

    def get_context_data(self, **kwargs):
        exam_report_context = super().get_context_data(**kwargs)
        report_detail = Exam_Report.objects.get(id=self.kwargs['pk'])
        pdf = str(report_detail.report_pdf).split("/")[-1]
        exam_report_context.update({"report_detail": report_detail,"pdf":pdf})
        print("report is {}".format(exam_report_context["report_detail"]))

        return exam_report_context

class ReportEditView(generic.UpdateView):
    model = Exam_Report
    template_name = "templates_araya/edit_report.html"
    form_class = EditReportForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_id = self.kwargs['pk']
        var = {"c_id": c_id}
        sql = 'select report_company.company_name,report_company.id ' \
              'from report_company,report_exam_report ' \
              'where %(c_id)s = report_exam_report.id and report_company.id = report_exam_report.report_company_id;'
        comp = Company.objects.raw(sql, var)
        context.update({"company": comp, })
        return context

    def get_success_url(self):
        return reverse_lazy('report:details_report', kwargs={'pk': self.kwargs["pk"]})
