from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render, resolve_url
from django.views import generic
from django.db.models import Q
import json
from ..models import *
# Create your views here.

"""
class IndexView(generic.TemplateView):
    template_name = "index.html"
"""

class Company_searchView(generic.ListView):
    model = Company,Offers,Tag,Tag_Company,Departments,Qualification,Big_Industry,Exam_Report
    template_name = "templates_ise/company_search.html"
    depq = "none"
    next_page = ""

    def get_context_data(self, *, object_list=None, **kwargs):
        def cnvDataToJson(object):
            return str(object)
        lists_context = super().get_context_data()
        quadep=Qualif_Departments.objects.all().values()
        encode_data = json.dumps(list(quadep), ensure_ascii=False, default=cnvDataToJson)

        non_show_departs_number = [6,]#学生の検索画面に表示させない{department}の項目リスト   [教務部,]
        lists_context.update({"departs":Departments.objects.filter(department_date=None),"industry":Big_Industry.objects.all(),"no_show":non_show_departs_number,'quadep':encode_data})
        return lists_context

    def post(self, request, *args,**kwargs):
        free_word = self.request.POST.get('query',"")
        qualifications = self.request.POST.getlist('qual_query',"")
        industry = self.request.POST.get('ind_query',"")
        locate_word = self.request.POST.get('locate_query',"")
        success = self.request.POST.get('passed',"")
        tag_word = self.request.POST.get('tag_query',"")
        next_page = self.request.POST.get('button',"")

        if next_page == "next":
            respond = HttpResponseRedirect(resolve_url("/result/"))
            dic = {"free_word":free_word,"qualifications":qualifications,"industry":industry,"locate_word":locate_word,"success":success,"tag_word":tag_word}
            for i in dic:
                respond.set_cookie(i,dic[i])
            return respond
        elif next_page == "back":
            return HttpResponseRedirect(resolve_url('/'),request.POST.get)
        else:pass

    def get_queryset(self):
        def cnvDataToJson(object):
            return str(object)

        qualif = Qualification.objects.all().values()
        encode_data = json.dumps(list(qualif), ensure_ascii=False, default=cnvDataToJson)
        print(encode_data)
        object_list = encode_data
        return object_list

class search_resultView(generic.ListView):
    template_name = "templates_ise/search_result.html"
    paginate_by = 30

    def get_queryset(self):
        global count
        condition = True
        will_list = []
        qualif_list = []
        tag_list = []
        ocption_list = []
        middle_list = []
        big_list = []
        cookie_list = {"free_word":"", "qualifications":"", "industry":"", "locate_word":"", "success":"", "tag_word":"", }
        for i in cookie_list:
            cookie_list[i] = self.request.COOKIES.get(i)

        try:
            if cookie_list["free_word"]:
                free_word = cookie_list["free_word"]
                condition = False
                for fq_word in list(set(free_word.split())):
                    will_list += Company.objects.filter(Q(company_name__icontains=fq_word) | Q(kana_name__icontains=fq_word) |Q(locate__icontains=fq_word) | Q(address_number__icontains=fq_word)).order_by("kana_name_normalized")
                    qualif_list += Qualification.objects.filter(Q(qualif_name__icontains=fq_word))
                    tag_list += Tag.objects.filter(Q(tag_name__icontains=fq_word))
                    ocption_list += Occupation.objects.filter(Q(occupation_name__icontains=fq_word))
                    middle_list += Middle_Industry.objects.filter(Q(middle_name__icontains=fq_word))
                    for ocptn in ocption_list:
                        middle_list += Middle_Industry.objects.filter(id=ocptn.middle_id)
                    big_list += Big_Industry.objects.filter(Q(big_name__icontains=fq_word))
                    for mdle in middle_list:
                        big_list += Big_Industry.objects.filter(big_id=mdle.big_id)

                buffer_list_freequal = []
                for qual in qualif_list:
                    buffer_list_freequal += Qualif_Compa.objects.filter(qualif_id=qual)
                for qual_compa in buffer_list_freequal:
                    will_list += Company.objects.filter(id=qual_compa.company_id).order_by("kana_name_normalized")

                buffer_list_freeind = []
                for ind in big_list:
                    buffer_list_freeind += Company_Industry.objects.filter(industry_id=ind)
                for ind_compa in buffer_list_freeind:
                    will_list += Company.objects.filter(id=ind_compa.company_id).order_by("kana_name_normalized")

                buffer_list_freetag = []
                for tag in tag_list:
                    buffer_list_freetag += Tag_Company.objects.filter(tag_id=tag)
                for tag_company in buffer_list_freetag:
                    will_list += Company.objects.filter(id=tag_company.company_id).order_by("kana_name_normalized")
            else:
                company_list = []
                try:
                    if cookie_list["qualifications"]:
                        qualifications = cookie_list["qualifications"]
                        condition = False
                        qcompany_list = []
                        buffer_list_qual = []
                        q = list(map(int,qualifications.replace('"',"").replace("[","").replace("]","").replace("'","").split(",")))
                        for qualif in q:
                            buffer_list_qual += list(Qualif_Compa.objects.filter(qualif_id=qualif))
                        for qualif_compa in buffer_list_qual:
                            qcompany_list += Company.objects.filter(id=qualif_compa.company_id).order_by("kana_name_normalized")
                        company_list += qcompany_list
                except:pass
                try:
                    if cookie_list["industry"]:
                        industry = cookie_list["industry"]
                        condition = False
                        icompany_list = []
                        buffer_list_comp_ind = list(Company_Industry.objects.filter(industry_id=industry))
                        for cmpy in buffer_list_comp_ind:
                            icompany_list += Company.objects.filter(id=cmpy.company_id).order_by("kana_name_normalized")
                        if company_list:
                            b_i_l = []
                            for bcl in company_list:
                                if bcl in icompany_list:
                                    b_i_l.append(bcl)
                            company_list = b_i_l
                        else:
                            company_list = icompany_list
                except:pass
                try:
                    if cookie_list["locate_word"]:
                        locate_word = cookie_list["locate_word"]
                        condition = False
                        lcompany_list = []
                        for locate in list(set(locate_word.split())):
                            lcompany_list += Company.objects.filter(Q(locate__icontains=locate)).order_by("kana_name_normalized")
                        if company_list:
                            l_l = []
                            for lcl in company_list:
                                if lcl in lcompany_list:
                                    l_l.append(lcl)
                            company_list = l_l
                        else:
                            company_list = lcompany_list
                except:pass
                try:
                    if cookie_list["success"] == 't':
                        condition = False
                        scompany_list = []
                        buffer_list_pass = Exam_Report.objects.filter(report_passed="t",report_tests="last")
                        for pasd in buffer_list_pass:
                            scompany_list += Company.objects.filter(id=pasd.report_company_id)
                        if company_list:
                            s_l = []
                            for scl in company_list:
                                if scl in scompany_list:
                                    s_l.append(scl)
                            company_list = s_l
                        else:
                            company_list = scompany_list
                except:pass
                try:
                    if cookie_list["tag_word"]:
                        tag_word = cookie_list["tag_word"]
                        condition = False
                        tcompany_list = []
                        for tq_word in list(set(tag_word.split())):
                            tag_list += Tag.objects.filter(Q(tag_name__icontains=tq_word))
                        buffer_list_tag = []
                        for tag in tag_list:
                            buffer_list_tag += list(Tag_Company.objects.filter(tag_id=tag))
                        for tag_company in buffer_list_tag:
                            tcompany_list += Company.objects.filter(id=tag_company.company_id).order_by("kana_name_normalized")
                        if company_list:
                            t_l = []
                            for tcl in company_list:
                                if tcl in tcompany_list:
                                    t_l.append(tcl)
                            company_list = t_l
                        else:
                            company_list = tcompany_list
                except:pass
                will_list += company_list
        except:pass
        will_list = list(set(will_list))
        if condition:
            will_list = Company.objects.all().order_by("kana_name_normalized")

        object_list = will_list
        count = len(will_list)
        return object_list

    def get_context_data(self, *, object_list=None, **kwargs):
        global count
        view_context = super().get_context_data(**kwargs)
        view_context.update({'count':count})
        return view_context

    def post(self,request,*args,**kwargs):
        prev = self.request.POST.get("previ")
        print(prev)
        stamp = prev.split("_")

        response = HttpResponseRedirect(resolve_url("/company_detail/{}".format(stamp[1])))
        response.set_cookie("previous",stamp[0])
        return response

class offers_listView(generic.TemplateView):
    template_name = "templates_ise/offers_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_id = self.kwargs['pk']
        var = {"c_id":c_id}
        sql = 'select * from report_offers,report_occupation,report_company ' \
              'where %(c_id)s = report_company.id and report_company.id = report_offers.company_id and ' \
              'report_offers.occupation_id = report_occupation.id;'
        of_oc = Offers.objects.raw(sql,var)
        context.update({"offers":of_oc,"company":Company.objects.filter(id=c_id)[0],"pk":c_id})
        return context


class all_reportView(generic.TemplateView):
    template_name = "templates_ise/report_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_id = self.kwargs['pk']
        var = {'c_id':c_id}
        sql = 'select * from report_company,report_exam_report ' \
              'where %(c_id)s = report_company.id and report_company.id = report_exam_report.report_company_id;'
        rpt = Exam_Report.objects.raw(sql,var)
        context.update({"reports":rpt,"company":Company.objects.filter(id=c_id)[0],"pk":c_id})
        return context

class company_informationView(generic.TemplateView):
    template_name = "templates_ise/company_information.html"

    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        company_list = self.kwargs['pk'] #pkとる
        company_detail = Company.objects.get(id=company_list) #pkとidが一致する企業

        tag_list = Tag_Company.objects.filter(company_id=company_list)#企業idがpkと一致するタグを取る
        qualif_list = Qualif_Compa.objects.filter(company_id=company_list)
        tags = []
        qualifs = []

        params ={'pk':self.kwargs['pk']}
        offer_occu_sql = ' select * from report_offers inner join report_occupation on report_offers.occupation_id = report_occupation.id and report_offers.company_id = %(pk)s  order by report_offers.id;'
        offer_occu = Offers.objects.raw(offer_occu_sql,params)

        for i in tag_list:
            tag_company = Tag.objects.get(id=i.tag_id)
            tags.append(tag_company)
        for i in qualif_list:
            qualif_compa = Qualification.objects.get(id=i.qualif_id)
            qualifs.append(qualif_compa)

        company_context.update({
            "company_detail": company_detail,
            "tags":tags,
            "qualifs":qualifs,
            "offers":offer_occu,
        })
        return company_context

    def post(self, request, *args, **kwargs):
        print(request.POST)
        if "create_er" in request.POST:
            url = resolve_url('report:create_exam_report',pk=self.kwargs['pk'])
            response = HttpResponseRedirect(url)
            response.set_cookie("detail","create_er")
            return response

"""
class output_reportView(generic.TemplateView):
    template_name = "templates_ise/report.html"


class job_offerView(generic.TemplateView):
    template_name = "templates_ise/job_offer.html"

"""