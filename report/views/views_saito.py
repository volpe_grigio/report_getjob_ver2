from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
# from report.forms_saito import SelectDepartForm
from accounts.models import Departments,User
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import(LoginView, LogoutView)
from report.forms import SignUpForm, LoginForm
from django.contrib.auth.hashers import make_password
from django.contrib.auth import login, authenticate
from django.http import HttpResponse



# Create your views here.

class IndexView(generic.TemplateView):
    model = User
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = User.objects.all()
        print(context)
        print(user)
        return context



class SignUpView(generic.FormView):
    template_name = "account/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy('report:menu')

    def post(self, request, *args, **kwargs):
        params = {'message':'', 'form':None}
        if request.method == 'POST':
            form = SignUpForm(request.POST, request.FILES)
            print(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                email = form.cleaned_data['email']
                password = make_password(form.cleaned_data['password'])
                department = form.cleaned_data['department']
                try:
                    # User.object.get(email = email)
                    user = User.objects.create(username=username, email=email, password=password, department=department)
                    user.save()
                except:
                    params['form'] = form
                    params['message'] = 'このユーザーは既に登録されています'
                    return render(request, 'account/signup.html', params)
                return redirect('report:login')
            return render(request, 'account/signup.html', {'form':form})


class LoginView(LoginView):
    form_class = LoginForm
    template_name = "account/login.html"
