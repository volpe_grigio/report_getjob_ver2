from django.http import *
from django.shortcuts import render, resolve_url
from django.urls import reverse_lazy
from django.views import generic
from ..models import *
from django.db.models import Q
from ..forms_nihei import *
from ..templates import *
# import pandas as pd
import webbrowser
import subprocess
import os
import docx
import datetime
# import datetime
# import csv
# Create your views here.
from tkinter import filedialog
import tkinter
from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, portrait
from reportlab.lib.units import inch, mm, cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from django.contrib import messages

class CompanyListView(generic.ListView):
    model = Company
    template_name = "templates_nihei/company_list.html"
    paginate_by = 30

    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        companies = Company.objects.all()
        companies_count = companies.count()
        a = companies

        q_word = self.request.GET.get('query')
        print(q_word)

        page_num = self.request.GET.get('page')
        if page_num is not None:
            aaa = 1 + 30 * (int(page_num[0]) - 1)
            bbb = 30 * (int(page_num[0]))
            if companies_count <= int(bbb):
                bbb = companies_count
            display_count = str(aaa) + "~" + str(bbb)
            company_context.update({
                "kensu": display_count,
            })
        else:
            aaa = 1
            bbb = 31
            if companies_count <= bbb:
                bbb = companies_count
            display_count = str(aaa) + "~" + str(bbb)
            company_context.update({
                "kensu": display_count,
                "qw": q_word
            })
        company_context.update({
            "company_list": a,
            "count": companies_count,
            "pnum": page_num,
            "qw": q_word
        })
        return company_context

    def get_queryset(self, **kwargs):
        q_word = self.request.GET.get('query')
        if q_word:
            object_list = Company.objects.filter(
                Q(company_name__icontains=q_word) | Q(locate__icontains=q_word) | Q(kana_name__icontains=q_word)
            ).order_by('kana_name_normalized')
        else:
            object_list = Company.objects.all().order_by('kana_name_normalized')
        return object_list

    def post(self, request, *args, **kwargs):
        print(request.POST)
        print(self.request.POST)
        if "tag_register" in request.POST:
            response = HttpResponseRedirect("/tag_register/")
            response.set_cookie("return", "list")
            return response


        if "list_" in request.POST["previ"]:
            previous = str(self.request.POST.get("previ")).split("_")
            respond = HttpResponseRedirect("/company_detail/{}".format(previous[1]))
            respond.set_cookie("previous","list")
            return respond


class CompanyListTwoView(generic.ListView):
    model = Company
    template_name = "templates_nihei/company_list2.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        print(object_list)
        context.update({
            "aaa": object_list
        })
        return context


class CompanyRegisterView(generic.CreateView):
    model = Company, Tag_Company, Tag
    template_name = "templates_nihei/company_register.html"
    form_class = CompanyRegisterForm
    form_class2 = CompanyAddTagForm
    form_class3 = CompanyQualificationForm
    success_url = reverse_lazy('report:company_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form2 = self.form_class2(self.request.GET or None)
        form3 = self.form_class3(self.request.GET or None)
        tag_list = Tag.objects.all()
        context.update({
            'form2': form2,
            'form3': form3,
            "tags": tag_list,
        })
        return context

    def form_valid(self, form):
        # messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def post(self, request, *args, **kwargs):
        if "list_modoru" in request.POST:
            response = HttpResponseRedirect('/company_list/')
            return response
        if "companya" in request.POST:
            response = HttpResponseRedirect("/tag_register/")
            response.set_cookie("return", "tag")
            return response

        # lstrip(' ').rstrip('　')
        company_name = request.POST['company_name'].replace(' ', '').replace('　', '')
        kana_name = request.POST["kana_name"].replace(' ', '').replace('　', '')
        locate = request.POST["locate"].replace(' ', '').replace('　', '')
        address_number = request.POST["address_number"].replace(' ', '').replace('　', '')
        kana_nor = request.POST["kana_name"].replace('カブシキガイシャ', '').replace(' ', '').replace('　', '')

        abac = Company(company_name=company_name, kana_name=kana_name, locate=locate, address_number=address_number,
                       kana_name_normalized=kana_nor)
        abac.save()

        company_id_desc = Company.objects.all().order_by('-id')
        id_list = []
        for i in company_id_desc:
            id_list.append(i.id)
        company_id = Company.objects.get(id=id_list[0])
        Tag_Company.company_id = company_id
        url = resolve_url('report:company_register_tag', pk=company_id.id)
        return HttpResponseRedirect(url)


class TagRegisterView(generic.CreateView):
    template_name = "templates_nihei/tag_register.html"
    model = Tag
    form_class = TagRegisterForm
    success_url = reverse_lazy('report:tag_register')

    def form_valid(self, form):
        # messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        tag_context = super().get_context_data(**kwargs)
        tag_list = Tag.objects.all()

        tag_context.update({
            "tags": tag_list,
        })
        return tag_context

    def post(self, request, *args, **kwargs):
        if "tag_del" in request.POST:
            response = HttpResponseRedirect("/tag_delete/")

            response.set_cookie("to_tagregi", "to_tag_register")

            return response

        if request.method == "POST":
            if "modoru" in request.POST:
                print(request.POST)

                if self.request.COOKIES.get("return") == "list":
                    return HttpResponseRedirect("/company_list/")

                else:
                    return HttpResponseRedirect("/company_register/")
        if "tag_name" in request.POST:
            aaa = request.POST["tag_name"]
            ab = Tag(tag_name=aaa)
            ab.save()

            return HttpResponseRedirect("/tag_register/")


class CompanyDetailView(generic.TemplateView):
    template_name = "templates_nihei/company_detail.html"
    model = Company, Offers, Tag, Tag_Company

    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        company_list = self.kwargs['pk']  # pkとる
        company_detail = Company.objects.get(id=company_list)  # pkとidが一致する企業

        tag_list = Tag_Company.objects.filter(company_id=company_list)  # 企業idがpkと一致するタグを取る
        qualif_list = Qualif_Compa.objects.filter(company_id=company_list)
        tags = []
        qualifs = []

        params = {'pk': self.kwargs['pk']}
        offer_occu_sql = ' select * from report_offers inner join report_occupation on report_offers.occupation_id = report_occupation.id and report_offers.company_id = %(pk)s  order by report_offers.id;'
        offer_occu = Offers.objects.raw(offer_occu_sql, params)

        for i in tag_list:
            tag_company = Tag.objects.get(id=i.tag_id)
            tags.append(tag_company)
        for i in qualif_list:
            qualif_compa = Qualification.objects.get(id=i.qualif_id)
            qualifs.append(qualif_compa)

        user = self.request.user

        previous_page = self.request.COOKIES.get("previous")

        company_context.update({
            "company_detail": company_detail,
            "tags": tags,
            "qualifs": qualifs,
            "offers": offer_occu,
            "department": user.department_id,
            "previous":previous_page,
        })
        return company_context

    def post(self, request, *args, **kwargs):
        print(request.POST)
        if "go_tag" in request.POST:
            url = resolve_url('report:company_add_tag', pk=self.kwargs['pk'])
            response = HttpResponseRedirect(url)
            response.set_cookie("detail", "go_tag")
            return response
        if "del_offer" in request.POST:
            url = resolve_url('report:offer_delete', pk=self.kwargs['pk'])
            response = HttpResponseRedirect(url)
            response.set_cookie("detail", "del_offer")
            return response
        if "create_er" in request.POST:
            url = resolve_url('report:create_exam_report', pk=self.kwargs['pk'])
            response = HttpResponseRedirect(url)
            response.set_cookie("detail", "create_er")
            return response

class OfferRegisterView(generic.CreateView):
    template_name = "templates_nihei/offer_register.html"
    model = Offers, Occupation, Company
    form_class = OfferRegisterForm
    success_url = reverse_lazy('report:company_detail')

    def get_success_url(self):
        return reverse_lazy('report:company_detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        offers = form.save(commit=False)
        # offers.company = self.kwargs['pk']
        offers.save()

        # messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        offer_register_context = super().get_context_data(**kwargs)
        occupation_list = Occupation.objects.all()

        offer_register_context.update({
            "occupation": occupation_list,
        })
        return offer_register_context

    def get(self, request, **kwargs):
        initial_dict = dict(company=self.kwargs['pk'])
        print(initial_dict)

        form = OfferRegisterForm(request.GET or None, initial=initial_dict)
        print(dict(form=form))
        return render(request, 'templates_nihei/offer_register.html', dict(form=form))


class OfferDeleteView(generic.TemplateView):
    template_name = "templates_nihei/offer_delete.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        params = {'pk': self.kwargs['pk']}
        offer_occu_sql = ' select * from report_offers inner join report_occupation on report_offers.occupation_id = report_occupation.id and report_offers.company_id = %(pk)s order by report_offers.id;'
        offer_occu = Offers.objects.raw(offer_occu_sql, params)

        context.update({
            "offers": offer_occu,
        })
        return context

    def post(self, request, *args, **kwargs):
        del_offers = request.POST.getlist('delete')
        url = ""
        if request.method == 'POST':
            if "modoru" in request.POST:
                if self.request.COOKIES.get("detail") == "del_offer":
                    url = resolve_url('report:company_detail', pk=self.kwargs['pk'])
            else:
                for i in del_offers:
                    Offers.objects.filter(id=i).delete()
                url = resolve_url('report:offer_delete', pk=self.kwargs['pk'])

        return HttpResponseRedirect(url)


class TagDeleteView(generic.ListView):
    template_name = "templates_nihei/tag_delete.html"
    model = Tag

    def get_context_data(self, **kwargs):
        tag_delete_context = super().get_context_data(**kwargs)

        tag_list = Tag.objects.all()

        tag_delete_context.update({
            "tags": tag_list,
        })
        return tag_delete_context

    def get_queryset(self):
        q_word = self.request.GET.get('query')
        if q_word:
            object_list = Tag.objects.filter(
                Q(tag_name__icontains=q_word))
        else:
            object_list = Tag.objects.all()

        return object_list

    def post(self, request):
        post_pks = request.POST.getlist('delete')  # <input type="checkbox" name="delete"のnameに対応

        if request.method == "POST":
            if "modoru" in request.POST:
                if self.request.COOKIES.get("to_tagregi") == "to_tag_register":
                    return HttpResponseRedirect("/tag_register/")


            else:
                aiuro = Tag_Company.objects.filter(tag_id__in=post_pks).delete()
                Tag.objects.filter(pk__in=post_pks).delete()
                return HttpResponseRedirect("/tag_delete/")


class CompanyAddTagView(generic.CreateView):
    template_name = "templates_nihei/company_add_tag.html"
    model = Tag_Company, Tag, Company
    form_class = CompanyAddTagForm

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tag_list = Tag.objects.all()
        company_name = Company.objects.get(id=self.kwargs['pk'])

        tagsr = Tag.objects.filter(tag_company__company_id=self.kwargs['pk'])
        tagss = Tag.objects.exclude(tag_company__company_id=self.kwargs['pk'])
        # tag_not_registerはqueryじゃないといけない

        context.update({
            "tags": tag_list,
            "company": company_name,
            "tag_register": tagsr,
            "tag_not_register": tagss,
        })
        return context

    def post(self, request, *args, **kwargs):
        url = ""
        if request.method == "POST":
            if "tag" in request.POST or "tagdel" in request.POST:
                add_tag_list = request.POST.getlist('tag')
                del_tag_list = request.POST.getlist('tagdel')
                company_id = Company.objects.get(id=self.kwargs['pk'])

                for i in add_tag_list:
                    tag_id = Tag.objects.get(id=i)
                    Tag_Company.company_id = company_id
                    Tag_Company.tag_id = i
                    tag_compa = Tag_Company(company=company_id, tag=tag_id)
                    tag_compa.save()

                for i in del_tag_list:
                    del_tag_id = Tag.objects.get(id=i)
                    Tag_Company.company_id = company_id
                    Tag_Company.tag_id = i
                    del_tag = Tag_Company.objects.get(company=company_id, tag=del_tag_id)
                    # del_tag = Tag_Company(id=i)
                    del_tag.delete()

                url = resolve_url('report:company_add_tag', pk=self.kwargs['pk'])
            if "add" in request.POST:
                if self.request.COOKIES.get("detail") == "go_tag":
                    url = resolve_url('report:company_detail', pk=self.kwargs['pk'])
        return HttpResponseRedirect(url)


class CompanyRegisterTagView(generic.ListView):
    template_name = "templates_nihei/company_register_tag.html"
    model = Tag

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        tag_list = Tag.objects.all()
        company_name = Company.objects.get(id=self.kwargs['pk'])

        context.update({
            "tags": tag_list,
            "company": company_name,
        })
        return context

    def post(self, request, *args, **kwargs):
        add_tag_list = request.POST.getlist('tag')
        company_id = Company.objects.get(id=self.kwargs['pk'])
        for i in add_tag_list:
            tag_id = Tag.objects.get(id=i)
            Tag_Company.company_id = company_id
            Tag_Company.tag_id = i
            tag_compa = Tag_Company(company=company_id, tag=tag_id)
            tag_compa.save()
        url = resolve_url('report:company_register_occupation', pk=self.kwargs['pk'])
        return HttpResponseRedirect(url)


class CompanyRegisterOccupationView(generic.TemplateView):
    template_name = "templates_nihei/company_register_occupation.html"
    model = Qualification, Company

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        company_name = Company.objects.get(id=self.kwargs['pk'])
        qualif_list = Qualification.objects.all()

        context.update({
            "qualifs": qualif_list,
            "company": company_name,
        })
        return context

    def post(self, request, *args, **kwargs):
        add_qualif_list = request.POST.getlist('qualif')
        company_id = Company.objects.get(id=self.kwargs['pk'])
        for i in add_qualif_list:
            qualif_id = Qualification.objects.get(id=i)
            Qualif_Compa.company_id = company_id
            Qualif_Compa.qualif_id = i
            qualif_compa = Qualif_Compa(qualif=qualif_id, company=company_id)
            qualif_compa.save()
        url = resolve_url('report:company_detail', pk=self.kwargs['pk'])
        return HttpResponseRedirect(url)


class LetterTemplateView(generic.TemplateView):
    template_name = "templates_nihei/letter_template.html"
    model = Company, Departments
    success_url = reverse_lazy('report:company_detail')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        if "create" in request.POST:
            aaa = request.POST.getlist('info')
            dohu = request.POST.getlist('dohu')
            user = self.request.user
            date = str(datetime.date.today()).split('-')  # 日付
            company = Company.objects.get(id=self.kwargs['pk'])
            depart = Departments.objects.get(id=user.department_id)

            # ユーザーネーム→user.username
            # 会社名→company.company_name
            # 学科名→depart.department_name
            # eメール→user.email
            # 担当者名　aaa[0]
            # 郵便番号　aaa[1]
            # 住所　aaa[2]
            # 電話番号　aaa[3]

            # 以下wordファイル書き換え
            doc = docx.Document("static/template/SampleA4.docx")
            # print("段落の個数：", len(doc.paragraphs))
            # print("表の個数：", len(doc.tables))
            # print("図（行内）の個数：", len(doc.inline_shapes))
            num = 0
            for para in doc.paragraphs:
                num = num + 1
                print(num, para.text)

            # 日付書き換え
            para1 = doc.paragraphs[0]
            t1 = para1.text
            t1 = t1.replace("Y", date[0]).replace("M", date[1]).replace("D", date[2])
            para1.text = t1
            print(t1)

            # 会社名書き換え
            para3 = doc.paragraphs[2]
            t3 = para3.text.replace("（会社名）", company.company_name)
            para3.text = t3
            print(t3)

            # 担当者名　書き換え
            para4 = doc.paragraphs[3]
            t4 = para4.text.replace("（担当者名）", aaa[0])
            para4.text = t4
            print(t4)

            # 学科学年書き換え
            para6 = doc.paragraphs[5]
            t6 = para6.text.replace("（学科学年）", depart.department_name + "2年")
            para6.text = t6
            print(t6)

            # 名前
            para7 = doc.paragraphs[6]
            t7 = para7.text.replace("（名前）", user.username)
            para7.text = t7

            # 郵便番号と住所書き換え
            para8 = doc.paragraphs[7]
            t8 = para8.text.replace("（郵便番号）", aaa[1]).replace("（住所）", aaa[2])
            para8.text = t8
            print(t8)

            # 電話番号書き換え
            para9 = doc.paragraphs[8]
            t9 = para9.text.replace("（電話番号）", aaa[3])
            para9.text = t9
            print(t9)

            # メールアドレス書き換え
            para10 = doc.paragraphs[9]
            t10 = para10.text.replace("（メールアドレス）", user.email)
            para10.text = t10
            print(t10)
            j = 0
            # 同封書類書き換え
            for i in range(26, 31):
                para = doc.paragraphs[i]
                if len(dohu) > j:
                    text = para.text.replace("（同封）", dohu[j])
                    j += 1
                    para.text = text + " " + "1通"
                else:
                    text = para.text.replace("（同封）", "")
                    j += 1
                    para.text = text


            # 簡易会社名
            compa_nor = company.company_name.replace("株式会社", "")
            #doc.save("送付状{0}{1}.docx".format(compa_nor, datetime.date.today()))
            filename = "送付状{0}{1}.docx".format(compa_nor, datetime.date.today())
            doc.save(filename)
            subprocess.Popen([r'C:\Program Files\Microsoft Office\Office16\WINWORD.EXE',
                              r"E:\venv_Report_GetJob\report_getjob_ver2\{0}".format(filename)])


            url = resolve_url('report:letter_template', pk=self.kwargs['pk'])
            return HttpResponseRedirect(url)


        if "return" in request.POST:
            url = resolve_url('report:result')
            return HttpResponseRedirect(url)


class CreateExamReportView(generic.TemplateView):
    template_name = "templates_nihei/create_exam_report.html"
    model = Company, User, Departments
    success_url = reverse_lazy('report:company_detail')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        company = Company.objects.get(id=self.kwargs['pk'])
        user = self.request.user
        print(user.department_id)
        context.update({
            "company": company.company_name,
            "department": user.department_id,
        })
        return context

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        url = ''

        if "cer" in request.POST:
            user = self.request.user
            username = user.username  # ユーザーネーム

            department = Departments.objects.get(id=user.department_id)
            depa = str(department).replace('科', '')  # 学科名

            year = datetime.date.today()
            str_year = str(year).split('-')
            reiwa1 = 2019
            nendo = ""
            if 1 <= int(str_year[1]) and int(str_year[1]) <= 3:
                nendo = (int(str_year[0]) - 1) - reiwa1 + 1
            else:
                nendo = int(str_year[0]) - reiwa1 + 1  # 年度

            year_kouhan = int(str_year[0]) - 2000

            company = Company.objects.get(id=self.kwargs['pk'])
            company_name = company.company_name  # 企業名
            company_locate = company.locate  # 住所
            company_address = str(company.address_number).split('-')
            company_address1 = company_address[0]  # 郵便番号　ハイフン前
            company_address2 = company_address[1]  # 郵便番号　ハイフン後
            grade = request.POST['grade']  # 学年

            seibetu = request.POST['seibetu']  # 性別

            writing_time = request.POST['writing_time']  # 筆記試験時間

            writing_content = request.POST.getlist('writing_content')  # 筆記試験内容
            commonsense = ""
            expert = ""
            japanese = ""
            math = ""
            english = ""
            writing_others = ""
            for i in writing_content:
                if i == "一般常識":
                    commonsense = i
                elif i == "専門":
                    expert = i
                elif i == "国語":
                    japanese = i
                elif i == "数学":
                    math = i
                elif i == "英語":
                    english = i
                else:
                    writing_others = i

            writing_question = request.POST['writing_question']  # 筆記試験問題
            len_wq = len(writing_question)
            waru = len_wq // 40
            sp_wq = writing_question.split("\r\n")
            wq = []
            if "\r\n" not in writing_question:
                for i in range(0, waru):
                    wq.append(writing_question[40 * i:40 + 40 * i])
                wq.append((writing_question[40 * waru:]))
            else:
                for i in range(0, len(sp_wq)):
                    if i < 8:
                        a_wq = sp_wq[i]
                        b_wq = len(sp_wq[i]) / 40
                        # b_wq = len(sp_wq[i]) // 40
                        if b_wq == 1:
                            wq.append(sp_wq[i])
                        elif b_wq > 1:
                            for j in range(0, int(b_wq)):
                                wq.append(a_wq[40 * j:40 + 40 * j])
                            wq.append(a_wq[40 * int(b_wq):])
                        else:
                            wq.append(sp_wq[i])

            composition_time = request.POST['composition_time']  # 作文時間

            composition_char = request.POST['composition_char']  # 作文字数

            composition_theme = request.POST['composition_theme']  # 作文テーマ

            aptitude_time = request.POST['aptitude_time']  # 適性試験時間

            aptitude_type = request.POST.getlist('aptitude_type')  # 適性試験種類
            personality = ""
            job = ""
            computer = ""
            spi = ""
            aptitude_others = ""
            for i in aptitude_type:
                if i == "性格":
                    personality = i
                elif i == "職業適性":
                    job = i
                elif i == "コンピュータ適性":
                    computer = i
                elif i == "SPI":
                    spi = i
                else:
                    aptitude_others = i

            aptitude_content = request.POST['aptitude_content']  # 適性試験内容
            len_ac = len(aptitude_content)
            waru_ac = len_ac // 40
            sp_ac = aptitude_content.split("\r\n")
            ac = []
            if "\r\n" not in aptitude_content:
                for i in range(0, waru_ac):
                    ac.append(aptitude_content[40 * i:40 + 40 * i])
                ac.append((aptitude_content[40 * waru:]))
            else:
                for i in range(0, len(sp_ac)):
                    if i < 4:
                        a_ac = sp_ac[i]
                        b_ac = len(sp_ac[i]) / 40
                        if b_ac == 1:
                            ac.append(sp_ac[i])
                        elif b_ac > 1:
                            for j in range(0, int(b_ac)):
                                ac.append(a_ac[40 * j:40 + 40 * j])
                            ac.append(a_ac[40 * int(b_ac):])
                        else:
                            ac.append(sp_ac[i])

            interview_time = request.POST['interview_time']  # 面接時間

            examiner = request.POST['examiner']  # 面接試験委員人数

            how = request.POST.getlist('how')  # 面接方法
            indivisual = ""
            group = ""
            interview_others = ""
            for i in how:
                if i == "個人面接":
                    indivisual = i
                elif i == "集団面接":
                    group = i
                else:
                    interview_others = i

            interview_content = request.POST['interview_content']  # 面接内容
            len_ic = len(interview_content)
            waru_ic = len_ic // 40
            sp_ic = interview_content.split("\r\n")
            ic = []
            if "\r\n" not in interview_content:
                for i in range(0, waru_ic):
                    ic.append(interview_content[40 * i:40 + 40 * i])
                ic.append((interview_content[40 * waru:]))
            else:
                for i in range(0, len(sp_ic)):
                    if i < 8:
                        a_ic = sp_ic[i]
                        b_ic = len(sp_ic[i]) / 40
                        if b_ic == 1:
                            ic.append(sp_ic[i])
                        elif b_ic > 1:
                            for j in range(0, int(b_ic)):
                                ic.append(a_ic[40 * j:40 + 40 * j])
                            ic.append(a_ic[40 * int(b_ic):])
                        else:
                            ic.append(sp_ic[i])

            health = request.POST['health']  # 健康診断
            interview_content = request.POST['interview_content']  # 面接内容
            len_ic = len(interview_content)
            waru_ic = len_ic // 40
            sp_ic = interview_content.split("\r\n")
            ic = []
            if "\r\n" not in interview_content:
                for i in range(0, waru_ic):
                    ic.append(interview_content[40 * i:40 + 40 * i])
                ic.append((interview_content[40 * waru:]))
            else:
                for i in range(0, len(sp_ic)):
                    if i < 8:
                        a_ic = sp_ic[i]
                        b_ic = len(sp_ic[i]) / 40
                        if b_ic == 1:
                            ic.append(sp_ic[i])
                        elif b_ic > 1:
                            for j in range(0, int(b_ic)):
                                ic.append(a_ic[40 * j:40 + 40 * j])
                            ic.append(a_ic[40 * int(b_ic):])
                        else:
                            ic.append(sp_ic[i])

            advice = request.POST['advice']  # 後輩への助言
            len_ad = len(advice)
            waru_ad = len_ad // 40
            sp_ad = advice.split("\r\n")
            ad = []
            if "\r\n" not in advice:
                for i in range(0, waru_ad):
                    ad.append(advice[40 * i:40 + 40 * i])
                ad.append((advice[40 * waru:]))
            else:
                for i in range(0, len(sp_ad)):
                    if i < 5:
                        a_ad = sp_ad[i]
                        b_ad = len(sp_ad[i]) / 40
                        if b_ad == 1:
                            ad.append(sp_ad[i])
                        elif b_ad > 1:
                            for j in range(0, int(b_ad)):
                                ad.append(a_ad[40 * j:40 + 40 * j])
                            ad.append(a_ad[40 * int(b_ad):])
                        else:
                            ad.append(sp_ad[i])

            aiueo = company_name.replace('株式会社', '')

            remarks = request.POST['remarks']
            len_rm = len(remarks)
            waru_rm = len_rm // 40
            sp_rm = remarks.split("\r\n")
            rm = []
            print(sp_rm)
            if "\r\n" not in remarks:
                for i in range(0, waru_rm):
                    rm.append(remarks[40 * i:40 + 40 * i])
                rm.append((remarks[40 * waru:]))
            else:
                for i in range(0, len(sp_rm)):
                    if i < 2:
                        a_rm = sp_rm[i]
                        b_rm = len(sp_rm[i]) / 40
                        if b_rm == 1:
                            rm.append(sp_rm[i])
                        elif b_rm > 1:
                            for j in range(0, int(b_rm)):
                                rm.append(a_rm[40 * j:40 + 40 * j])
                            rm.append(a_rm[40 * int(b_rm):])
                        else:
                            rm.append(sp_rm[i])

            template_file = 'static/template/exam_sample.pdf'
            output_file = '受験報告書{0}{1}{2}.pdf'.format(aiueo, username, str_year[0])
            tmp_file = '__tmp.pdf'
            tmp_file2 = '__tmp2.pdf'
            hogan_file = '__hogan.pdf'

            # A4縦のCanvasを作成 -- (*1)
            w, h = portrait(A4)
            cv = canvas.Canvas(tmp_file, pagesize=(w, h))
            # フォントを登録しCanvasに設定 --- (*2)
            ttf_file = 'static/template//ipaexg.ttf'
            pdfmetrics.registerFont(TTFont('IPAexGothic', ttf_file))
            font_size = 8.5
            cv.setFont('IPAexGothic', font_size)
            name = username
            # 文字列を描画する --- (*3)
            cv.setFillColorRGB(0, 0, 0)
            cv.drawString(55 * mm, 241.5 * mm, depa)
            font_size = 11
            cv.setFont('IPAexGothic', font_size)
            cv.drawString(140 * mm, 241 * mm, name)
            cv.drawString(33 * mm, 278.3 * mm, str(nendo))
            cv.drawString(45.5 * mm, 229 * mm, company_name)
            cv.drawString(55.5 * mm, 219.5 * mm, company_address1)
            cv.drawString(75.5 * mm, 219.5 * mm, company_address2)
            cv.drawString(45.5 * mm, 212 * mm, company_locate)
            cv.drawString(45.5 * mm, 241 * mm, request.POST['grade'])
            cv.drawString(145 * mm, 249.7 * mm, str(year_kouhan))
            cv.drawString(160 * mm, 249.7 * mm, str(str_year[1]))
            cv.drawString(176 * mm, 249.7 * mm, str(str_year[2]))
            font_size = 18
            cv.setFont('IPAexGothic', font_size)
            if request.POST['seibetu'] == '男':
                cv.drawString(180 * mm, 240.2 * mm, "〇")
            elif request.POST['seibetu'] == '女':
                cv.drawString(185.6 * mm, 240.2 * mm, "〇")
            # 一時ファイルに保存 --- (*4)
            cv.showPage()
            cv.save()

            # 2㌻目
            cv2 = canvas.Canvas('__tmp2.pdf', pagesize=(w, h))
            font_size = 7
            cv2.setFont('IPAexGothic', font_size)
            if aptitude_others:
                cv2.drawString(158 * mm, 199.5 * mm, aptitude_others)
            if interview_others:
                cv2.drawString(159 * mm, 167.2 * mm, interview_others)

            font_size = 8
            cv2.setFont('IPAexGothic', font_size)
            if writing_others:
                cv2.drawString(153 * mm, 265.5 * mm, writing_others)

            font_size = 11
            cv2.setFont('IPAexGothic', font_size)
            cv2.setFillColorRGB(0, 0, 0)
            cv2.drawString(50 * mm, 276.5 * mm, company_name)
            cv2.drawString(40 * mm, 265.3 * mm, writing_time)
            cv2.drawString(40 * mm, 208 * mm, composition_time)
            cv2.drawString(72 * mm, 208 * mm, composition_char)
            cv2.drawString(110 * mm, 208 * mm, composition_theme)
            cv2.drawString(40 * mm, 199 * mm, aptitude_time)
            for i in range(0, len(wq)):
                a = wq[i]
                cv2.drawString(29 * mm, (250 - 5 * i) * mm, a)
            for i in range(0, len(ac)):
                a = ac[i]
                cv2.drawString(29 * mm, (190 - 5 * i) * mm, a)
            for i in range(0, len(ic)):
                a = ic[i]
                cv2.drawString(29 * mm, (152 - 8.58 * i) * mm, a)
            for i in range(0, len(ad)):
                a = ad[i]
                cv2.drawString(24.5 * mm, (63 - 8.38 * i) * mm, a)
            for i in range(0, len(rm)):
                a = rm[i]
                cv2.drawString(29 * mm, (22 - 5 * i) * mm, a)
            cv2.drawString(40 * mm, 167.2 * mm, interview_time)
            cv2.drawString(80 * mm, 167.2 * mm, examiner)
            cv2.drawString(29 * mm, 82 * mm, health)
            # cv2.drawString(24.5*mm,62*mm,"０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９")

            font_size = 22
            cv2.setFont('IPAexGothic', font_size)
            # 必死危険の内容
            if commonsense != "":
                cv2.drawString(80 * mm, 264 * mm, "〇")
            if expert != "":
                cv2.drawString(94 * mm, 264 * mm, "〇")
            if japanese != "":
                cv2.drawString(105 * mm, 264 * mm, "〇")
            if math != "":
                cv2.drawString(115.8 * mm, 264 * mm, "〇")
            if english != "":
                cv2.drawString(126.8 * mm, 264 * mm, "〇")

            # 適性試験の内容
            if personality:
                cv2.drawString(75 * mm, 197.7 * mm, "〇")
            if job:
                cv2.drawString(90 * mm, 197.7 * mm, "〇")
            if computer:
                cv2.drawString(112 * mm, 197.7 * mm, "〇")
            if spi:
                cv2.drawString(129.5 * mm, 197.7 * mm, "〇")

            # 面接試験の方法
            if indivisual:
                cv2.drawString(110 * mm, 166 * mm, "〇")
            if group:
                cv2.drawString(130 * mm, 166 * mm, "〇")

            cv2.showPage()
            cv2.save()

            cv_hogan = canvas.Canvas('__hogan.pdf', pagesize=(w, h))
            # 方眼紙のマスを計算 --- (*2)
            rsize = 10 * mm
            # マスのサイズ
            count_x = int(w / rsize)
            count_y = int(h / rsize)
            top_x = int((w - count_x * rsize) / 2)
            # top_y = margin_y
            top_y = 0
            bottom_x = top_x + count_x * rsize
            bottom_y = top_y + count_y * rsize
            # ヘッダに英数文字を描画 ---
            # (*3)
            cv_hogan.setFont("Times-Roman", 10)
            # cv2.drawString(top_x, bottom_y+5, "A4 Graph Paper 12mm")
            # 線の色をRGBで指定 --- (*4)
            cv_hogan.setStrokeColorRGB(0.4, 0.6, 0.8)
            # 線のサイズを指定
            cv_hogan.setLineWidth(1)
            # 方眼紙の線を描画 --- (*5)
            for y in range(count_y + 1):
                ry = y * rsize + top_y
                cv_hogan.line(top_x, ry, bottom_x, ry)
            for x in range(count_x + 1):
                rx = x * rsize + top_x
                cv_hogan.line(rx, top_y, rx, bottom_y)
            # ファイルに保存
            cv_hogan.showPage()
            cv_hogan.save()

            # 1㌻目のもとのやつ
            template_pdf = PdfFileReader(template_file)
            template_page = template_pdf.getPage(0)
            tmp_pdf = PdfFileReader(tmp_file)
            template_page.mergePage(tmp_pdf.getPage(0))

            # １ぺーじめのほうがん
            # template_pdf = PdfFileReader(template_file)
            # template_page = template_pdf.getPage(0)
            #
            # hogan_pdf = PdfFileReader(hogan_file)
            # template_page.mergePage(hogan_pdf.getPage(0))

            # ２ぺーじめのもとのやつ
            template_pdf = PdfFileReader(template_file)
            template_page2 = template_pdf.getPage(1)

            tmp_pdf2 = PdfFileReader(tmp_file2)
            template_page2.mergePage(tmp_pdf2.getPage(0))

            # ２ぺーじめのはうがん
            # template_pdf = PdfFileReader(template_file)
            # template_page2 = template_pdf.getPage(1)
            #
            # hogan_pdf = PdfFileReader(hogan_file)
            # template_page2.mergePage(hogan_pdf.getPage(0))

            output = PdfFileWriter()
            output.addPage(template_page)
            output.addPage(template_page2)
            # with open(output_file, "wb") as fp:
            #     output.write(fp)

            filename = filedialog.asksaveasfilename(
                title="名前を付けて保存",
                filetypes=[("PDF", ".pdf")],
                defaultextension="pdf"
            )

            try:
                with open(filename,"wb") as fp:
                    output.write(fp)
            except:
                print('error')
            root = tkinter.Tk()
            root.mainloop()

        if "modoru" in request.POST:
            if self.request.COOKIES.get("detail") == "create_er":
                url = resolve_url('report:company_detail', pk=self.kwargs['pk'])
        return HttpResponseRedirect(url)