from django.urls import path,include
from .views import views_saito

app_name = 'report'
urlpatterns = [
    path('signup/',views_saito.SignUpView.as_view(),name='signup'),
    path('login/',views_saito.LoginView.as_view(),name='login'),

]
